package prg1210aldarias;

class CustomThread1 implements Runnable {

  String nombre;

  CustomThread1(String n) {
    nombre = n;
  }

  public void run() {
    // Instrucciones para el que se creo
    System.out.println("Hilo " + nombre);
  }

  public static void main(String args[]) {
    // Creamos los hilos
    CustomThread1 s1 = new CustomThread1("Thread-0");
    Thread t1 = new Thread(s1);
    t1.start();
    CustomThread1 s2 = new CustomThread1("Thread-1");
    Thread t2 = new Thread(s2);
    t2.start();
  }
}
/* EJECUCION
 Hilo Thread-0
 Hilo Thread-1
 */
