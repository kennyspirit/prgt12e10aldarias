package prg1210aldarias;

class CustomThread extends Thread {

  CustomThread(String name) {
    super(name);
    start();
  }

  public void run() {
    try {
      for (int loop_index = 0; loop_index < 2; loop_index++) {
        System.out.println((Thread.currentThread()).getName()
                + " hilo aqui...");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
    }
    System.out.println((Thread.currentThread()).getName()
            + " finaliza.");
  }
}

class isalive {

  public static void main(String args[]) {
    CustomThread thread1 = new CustomThread("primer");
    CustomThread thread2 = new CustomThread("segundo");
    System.out.println(thread1.isAlive());
    try {
      thread1.join();
      thread2.join();
    } catch (InterruptedException e) {
    }
    System.out.println(thread1.isAlive());
  }
}

/* EJECUCION:
 primer hilo aqui...
 true
 segundo hilo aqui...
 primer hilo aqui...
 segundo hilo aqui...
 primer finaliza.
 segundo finaliza.
 false
 */
