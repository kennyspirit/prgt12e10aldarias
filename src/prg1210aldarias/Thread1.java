package prg1210aldarias;

class Thread1 extends Thread {

  Thread1() {
    // Constructor de inicio
    start();
  }

  public void run() {
    // Instrucciones para el que se creo
    System.out.println("Hilo " + this.getName());
  }

  public static void main(String args[]) {
    // Creamos los hilos
    Thread1 thread1 = new Thread1();
    Thread1 thread2 = new Thread1();
  }
}
/* EJECUCION
 Hilo Thread-0
 Hilo Thread-1
 */
