package prg1210aldarias;

class CustomThread2 implements Runnable {

  Thread thread;

  CustomThread2() {
    thread = new Thread(this);
    thread.start();
  }

  public void run() {
    // Instrucciones para el que se creo
    System.out.println("Hilo " + thread.getName());
  }

  public static void main(String args[]) {
    // Creamos los hilos
    CustomThread2 s1 = new CustomThread2();
    CustomThread2 s2 = new CustomThread2();
  }
}
/* EJECUCION
 Hilo Thread-0
 Hilo Thread-1
 */
