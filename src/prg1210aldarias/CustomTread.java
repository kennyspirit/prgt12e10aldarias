package prg1210aldarias;

/**
 * Este programa muestra la instanciacion y ejecucion de tareas o hilos de
 * ejeccucion utilizando el interfaz Runnable en vez de extender la clase Thread
 */
class HilosMultiples01 {

  static public void main(String args[]) {
    // Se instancia dos nuevos objetos Thread
    Thread hiloA = new Thread(new MiHilo(), "hiloA");
    Thread hiloB = new Thread(new MiHilo(), "hiloB");
    // Se arrancan las dos tareas, para que comiencen su ejecucion
    hiloA.start();
    hiloB.start();
    // Aqui se retrasa la ejecucion un segundo y se captura la
    // posible excepcion que genera el metodo, aunque no se hace
    // nada en el caso de que se produzca
    try {
      Thread.currentThread().sleep(1000);
    } catch (InterruptedException e) {
    }
    // Presenta informacion acerca del Thread o hilo principal
    // del programa
    System.out.println(Thread.currentThread());
  }
}
