package prg1210aldarias;

class Thread4 {

  public static void main(String args[]) {
    // Creamos los hilos
    Thread t1 = new Thread(new miHilo4(), "Thread-1");
    Thread t2 = new Thread(new miHilo4(), "Thread-2");
    t1.start();
    t2.start();
  }
}

class miHilo4 extends Thread {
  // Sobreescribimos run

  public void run() {
    System.out.println("Hilo " + Thread.currentThread().getName());
  }
}

/* EJECUCION:
 Hilo Thread-1
 Hilo Thread-2
 */
