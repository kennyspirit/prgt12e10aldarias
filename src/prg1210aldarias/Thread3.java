package prg1210aldarias;

class Thread3 {

  public static void main(String args[]) {
    // Creamos los hilos. 
    // Se puede quitar el 2do  parametro de Thread
    Thread t1 = new Thread(new miHilo3(), "Thread-1");
    Thread t2 = new Thread(new miHilo3(), "Thread-2");
    t1.start();
    t2.start();
  }
}

class miHilo3 implements Runnable {

  public void run() {
    System.out.println("Hilo " + Thread.currentThread().getName());
  }
}

/* EJECUCION:
 Hilo Thread-1
 Hilo Thread-2
 */
