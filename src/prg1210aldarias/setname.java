package prg1210aldarias;

class setname {

  public static void main(String args[]) {
    Thread thread = Thread.currentThread();
    System.out.println("Nombre hilo principal: "
            + thread.getName());
    thread.setName("Nuevo");
    System.out.println("Nombre del hilo principal:"
            + thread.getName());
  }
}
/* EJECUCION:
 Nombre hilo principal: main
 Nombre del hilo principal:Nuevo
 */
