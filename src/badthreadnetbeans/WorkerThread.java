package badthreadnetbeans;

import java.util.Random;
import javax.swing.DefaultListModel;

/**
 * @author pacoaldarias <paco.aldarias@ceedcv.es>
 */
class WorkerThread extends Thread {

  private DefaultListModel model;
  private Random generator;

  public WorkerThread(DefaultListModel aModel) {
    model = aModel;
    generator = new Random();
  }

  public void run() {

    for (int j = 0; j <= 10; j++) {
      //while (true) {
      Integer i = new Integer(generator.nextInt(10));
      if (model.contains(i)) {
        model.removeElement(i);
      } else {
        model.addElement(i);
      }

    }
  }
}
