package swingWorker;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 * @author pacoaldarias <paco.aldarias@ceedcv.es>
 */
public class IncrementarSinSwingWorkerAction
        extends AbstractAction {

  private Main ejemploFrame;

  public IncrementarSinSwingWorkerAction(
          Main ejemploSinSwingWorkerFrame) {
    this.ejemploFrame = ejemploSinSwingWorkerFrame;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    int ite = 0;
    while (ite < Main.MAX_ITE) {
      ite = ite + 1;
      this.ejemploFrame.getTextField().setText("" + ite);
    }
  }
}
